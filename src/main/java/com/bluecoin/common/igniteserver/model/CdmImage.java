package com.bluecoin.common.igniteserver.model;

import lombok.Data;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

@Data
public class CdmImage implements Serializable {

	/**
	 * 
	 */
	@QuerySqlField
	private Integer id;
	
	@QuerySqlField
	private byte[] image;
	
	@QuerySqlField
	private String mimeType;
	
	@QuerySqlField
	private Integer width;
	
	@QuerySqlField
	private Integer height;

}
