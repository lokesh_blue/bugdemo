package com.bluecoin.common.igniteserver.config;

import com.bluecoin.common.igniteserver.model.*;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataRegionConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Thread.sleep;

@Configuration
public class IgniteConfig {

    @Value("${ignite.instance.name}")
    private String clusterName;

    @Value("${ignite.client.mode:false}")
    private Boolean clientMode;
    
    @Value("${ignite.ram.maxsize:1000}")
    private Long maxRAMSize;

    @Value("${ignite.ram.initsize:512}")
    private Integer initRAMSize;

    @Value("${ignite.multicastgroup:224.0.0.0}")
    private String multicastGroup;

    @Value("${ignite.networkdiscovery:true}")
    private Boolean networkDiscovery;

    public IgniteAtomicLong idGenerator;

    @Bean(name = "igniteInstance")
    public Ignite igniteInstance() {
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setClientMode(clientMode);
        cfg.setIgniteInstanceName(clusterName);

        // Storage Config
        DataStorageConfiguration storageCfg = new DataStorageConfiguration();
        storageCfg.getDefaultDataRegionConfiguration().setMaxSize(maxRAMSize * 1024 * 1024);
        storageCfg.getDefaultDataRegionConfiguration().setInitialSize(initRAMSize * 1024 * 1024);
        cfg.setDataStorageConfiguration(storageCfg);

        DataStorageConfiguration storageCfgPresistent = new DataStorageConfiguration();
        DataRegionConfiguration regionCfg = new DataRegionConfiguration();
        regionCfg.setName("Persistent");
        regionCfg.setInitialSize(100L * 1024 * 1024);
        regionCfg.setMaxSize(500L * 1024 * 1024);
//        regionCfg.setPersistenceEnabled(true);
        storageCfgPresistent.setDataRegionConfigurations(regionCfg);
        cfg.setDataStorageConfiguration(storageCfgPresistent);

        // Discovery Config
        if (networkDiscovery) {
            TcpDiscoverySpi spi = new TcpDiscoverySpi();
            TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
            ipFinder.setMulticastGroup(multicastGroup);
            spi.setIpFinder(ipFinder);
            cfg.setDiscoverySpi(spi);
        } else {
            TcpDiscoverySpi spi = new TcpDiscoverySpi();
            TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
            ipFinder.setAddresses(Arrays.asList("localhost"));
            spi.setIpFinder(ipFinder);
            cfg.setDiscoverySpi(spi);
        }


        CacheConfiguration ccfg1 = new CacheConfiguration("CdmImage");
        ccfg1.setIndexedTypes(Integer.class, CdmImage.class);
        ccfg1.setBackups(1);
        cfg.setCacheConfiguration(ccfg1);

        Ignite ignite = Ignition.start(cfg);
        idGenerator = ignite.atomicLong("id", 0, true);
        return ignite;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void test() throws InterruptedException {
        // This should be executed when application starts
        while (true) {
            System.out.println("\n\n\n\n\n\nworking\n\n\n\n\n");
            sleep(1000);
        }
    }
}

