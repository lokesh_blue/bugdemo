package com.bluecoin.common.igniteserver;

import org.apache.ignite.springdata20.repository.config.EnableIgniteRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableIgniteRepositories
public class IgniteserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(IgniteserverApplication.class, args);
	}
}
